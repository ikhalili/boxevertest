var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var readline = require('readline');

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("What is the page to be scraped?\n 1-eDreams \n 2-Opodo \n 3-emptyPage \n\n", function(answer) {
    // TODO: Log the answer in a database
  //  console.log("Thank you for your valuable feedback:", answer);

    function extractData(index,elem){


        var rawData = $(elem).data('flight');
        var duration =  $(elem).data('duration');
        var direct =  $(elem).data('flightNumber').split(",").length == 2;

        return {

            departureTime       : rawData.dtime,
            arrivalTime         : rawData.atime,
            carrieCode          : rawData.carriercode,
            durationInMinutes   : duration,
            direct              : direct


        };
    }

    var content ={};

    switch(parseInt(answer)) {
        case 1:
            content = fs.readFileSync("../pages/eDreams.html",{encoding:"utf8"});
            console.log('eDreams page to be scraped');
            break;
        case 2:
            content = fs.readFileSync("../pages/Opodo.html",{encoding:"utf8"});
            console.log('Opodo page to be scraped');
            break;
        case 3:
            content = fs.readFileSync("../pages/emptyPage.html",{encoding:"utf8"});
            console.log('empty page to be scraped');
            break;
        default:
            console.log('non existent page');
            break;
    }

    var  $ = cheerio.load(content);


    var dataModel = {};


    var staticInfo = $('.od-searchSummary-box-text').map((index, element) => $(element).text().trim()).get();

    dataModel.from = staticInfo[0];
    dataModel.to = staticInfo[1];
    dataModel.departure_date = staticInfo[2];
    dataModel.return_date = staticInfo[3];
    dataModel.num_passengers = staticInfo[4];


    dataModel.flightItems= $('.result').map((index, element) => {

        var price = $(element).data('price');
    var flightInfo = {};
    flightInfo.departure = $(element).find('[data-itinerary-group-id=1] .itinerary_row').map(extractData).get();
    flightInfo.return = $(element).find('[data-itinerary-group-id=2] .itinerary_row').map(extractData).get();

    flightInfo.price = price;

    return flightInfo;
}).get();



    if (dataModel.flightItems.length === 0 ){

        request.post({
            headers: {'content-type': 'application/json'},
            url: 'http://127.0.0.1:8080/sendFault',
            body: JSON.stringify({errorCode: "500", errorMessage: "Error in the script"})
        }).on('error',function(error){

            console.log('Server not available, check if it is running');

        })
            .on('complete',function(response){
                console.log('Error  Message sent');
            });
    }
    else
    {

        request.post({
            headers: {'Content-type': 'application/json'},
            url: 'http://127.0.0.1:8080/sendScrapedData',
            body: JSON.stringify(dataModel)
        }).on('error',function(error){

            console.log('Server not available, check if it is running');

        })
            .on('complete',function(response){
                console.log('Data Message sent');
            });
    }

    rl.close();
});







