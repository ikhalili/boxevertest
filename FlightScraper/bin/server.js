var express = require('express');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.all('/*', function(req, res, next) {
    // CORS headers
    res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    // Set custom headers for CORS
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

app.post('/sendScrapedData', function(req, res){

    console.log(JSON.stringify(req.body));

        res.sendStatus(200).end();
});

app.post('/sendFault', function(req, res){

    console.log(JSON.stringify(req.body));

    res.sendStatus(200).end();
});

app.listen('8080')
exports = module.exports = app;