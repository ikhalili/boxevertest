# Assumptions

1. The pages to be scraped have been saved offline for ease of use.
2. The backend server has been simulated with a simple express server (see server.js)
3. The booking flight pages taken into consideration are: Opodo and eDreams
4. The script runs on Node.js as well as on the browser aprt for the post request( due to the security constraint on the websites, Cross-Origin)

# Project Structure

```
FlightScraper
    |
    |
    |_ _ _ _  bin: the backend server
    |
    |
    |_ _ _ _  node_modules: library dependecies
    |
    |
    |_ _ _ _  pages: offline HTML pages
    |
    |
    |
    |_ _ _ _  src: website_one_script.js (main script)
    |
    |
    |
    |_ _ _ _  tetPlan: TestCases.xlsx, representing the test plan
    
```